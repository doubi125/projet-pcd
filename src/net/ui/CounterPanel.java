package net.ui;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class CounterPanel extends JPanel implements ActionListener
{
	JButton minusButton = new JButton("-"), plusButton = new JButton("+");
	JLabel minLabel, maxLabel, valueLabel;
	int max, min, val;
	
	public CounterPanel(int min, int val, int max)
	{
		this.min = min;
		this.max = max;
		this.val = val;
		minLabel = new JLabel("min : " + min);
		maxLabel = new JLabel("max : " + max);
		valueLabel = new JLabel(""+val);
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(minLabel);
		add(Box.createHorizontalStrut(5));
		JPanel minusPanel = new JPanel(new GridLayout(1, 1));
			minusPanel.setPreferredSize(new Dimension(50, 50));
			minusButton.addActionListener(this);
			minusPanel.add(minusButton);
		add(minusPanel);
		add(Box.createHorizontalStrut(5));
		JPanel valuePanel = new JPanel(new GridBagLayout());
			valuePanel.setBorder(BorderFactory.createRaisedBevelBorder());
			valuePanel.setPreferredSize(new Dimension(50, 50));
			valuePanel.setMinimumSize(new Dimension(50, 50));
			valuePanel.add(valueLabel);
		add(valuePanel);
		plusButton.addActionListener(this);
		add(Box.createHorizontalStrut(5));
		JPanel plusPanel = new JPanel(new GridLayout(1, 1));
			plusPanel.setPreferredSize(new Dimension(50, 50));
			plusPanel.add(plusButton);
		add(plusPanel);
		add(Box.createHorizontalStrut(5));
		add(maxLabel);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		if(arg0.getSource() == minusButton)
			addValue(-1);
		if(arg0.getSource() == plusButton)
			addValue(1);
	}
	
	public void addValue(int quantity)
	{
		val += quantity;
		if(val < min)
			val = min;
		if(val > max)
			val = max;
		valueLabel.setText(""+val);
	}
	
	public void setBounds(int min, int max)
	{
		this.min = min;
		this.max = max;
		if(val < min)
			val = min;
		if(val > max)
			val = max;
		valueLabel.setText(""+val);
		maxLabel.setText("max : " + max);
		
	}

	public void setMinValue(int min)
	{
		this.min = min;
		if(val < min)
			val = min;
		if(min > max)
			max = min;
		minLabel.setText("min : " + min);
		maxLabel.setText("max : " + max);
		valueLabel.setText("" + val);
	}

	public void setMaxValue(int max)
	{
		this.max = max;
		if(val > max)
			val = max;
		if(min > max)
			min = max;
		minLabel.setText("min : " + min);
		maxLabel.setText("max : " + max);
		valueLabel.setText("" + val);
	}
}
