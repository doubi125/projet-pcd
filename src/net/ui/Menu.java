package net.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class Menu extends JMenuBar implements ActionListener
{
	private JMenuItem minusItem, plusItem, exitItem, minItem, maxItem;
	private CounterPanel counter;
	
	public Menu(CounterPanel counter)
	{
		this.counter = counter;

		JMenu fileMenu = new JMenu("Fichier");
			exitItem = new JMenuItem("Quitter");
			exitItem.addActionListener(this);
			fileMenu.add(exitItem);
		add(fileMenu);
		
		JMenu modifMenu = new JMenu("Modifier");
			plusItem = new JMenuItem("Ajouter");
			plusItem.addActionListener(this);
			minusItem = new JMenuItem("Soustraire");
			minusItem.addActionListener(this);
			modifMenu.add(plusItem);
			modifMenu.add(minusItem);
		add(modifMenu);
		JMenu boundsMenu = new JMenu("Intervalle");
			minItem = new JMenuItem("Définir valeur minimale");
			minItem.addActionListener(this);
			maxItem = new JMenuItem("Définir valeur maximale");
			maxItem.addActionListener(this);
			boundsMenu.add(minItem);
			boundsMenu.add(maxItem);
		add(boundsMenu);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == plusItem)
			counter.addValue(1);
		else if(e.getSource() == minusItem)
			counter.addValue(-1);
		else if(e.getSource() == exitItem)
			System.exit(0);
		else if(e.getSource() == minItem)
		{
			String s = (String)JOptionPane.showInputDialog(
                    null,
                    "Valeur minimale :\n",
                    "min",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "");
			if(s == null)
				return;
			Integer val = Integer.parseInt(s);
			counter.setMinValue(val);
		}
		else if(e.getSource() == maxItem)
		{
			String s = (String)JOptionPane.showInputDialog(
                    null,
                    "Valeur maximale :\n",
                    "max",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "");
			if(s == null)
				return;
			Integer val = Integer.parseInt(s);
			counter.setMaxValue(val);
		}
	}
}
