package net.ui;

import javax.swing.JFrame;

public class MainFrame extends JFrame
{
	public static void main(String args[])
	{
		MainFrame frame = new MainFrame();
		frame.setVisible(true);
	}
	
	public MainFrame()
	{
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		CounterPanel panel = new CounterPanel(0, 5, 10);
		setContentPane(panel);
		setJMenuBar(new Menu(panel));
		pack();
	}
}
